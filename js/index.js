const MUSIC_LIST = [
    {
        "img": "./images/Faded.jpg",
        "name": "Faded",
        "artist": "Alan Walker",
        "music": "./musics/Fade.mp3"
    },
    {
        "img": "./images/Lily.png",
        "name": "Lily",
        "artist": "Alan Walker",
        "music": "./musics/Lily.mp3"
    },
    {
        "img": "./images/TheNights.jpg",
        "name": "The Night",
        "artist": "Avicii (Cover by AngieN.)",
        "music": "./musics/TheNights.mp3"
    },
    {
        "img": "./images/VuHoiHoaTrang.jpg",
        "name": "假面舞会",
        "artist": "很美味",
        "music": "./musics/VuHoiHoaTrang.mp3"
    }
]
    const picture=document.querySelector(".picture")
    const play=document.getElementById("play")
    const pause=document.getElementById("pause")
    const status1=document.querySelector(".status")
    const nameSong=document.querySelector(".nameSong")
    const nameSinger=document.querySelector(".nameSinger")
    const currentTrack=document.createElement("audio")
    const volumeRanger=document.querySelector(".volumeRanger")
    const seek=document.querySelector(".seek")
    const currentTime=document.querySelector(".currentTime")
    const durationTime=document.querySelector(".durationTime")
    let i=1
    var id
    // console.log(picture)
    const handleRight=()=>{
        i=i+1
        if(i===numberSong+1){
            i=1
        }
        status1.innerHTML=`Playing music ${i} of ${numberSong}`
        nameSong.innerHTML=MUSIC_LIST[i-1]["name"]
        nameSinger.innerHTML=MUSIC_LIST[i-1]["artist"]
        loadTrack(i)
        handleStop()
    }

    function loadTrack(i){
        reset()
        clearInterval(id)
        currentTrack.src=MUSIC_LIST[i-1]["music"]
        currentTrack.load()
        id=setInterval(setUpdate,1000)
        currentTrack.addEventListener('ended', handleRight);
    }
    loadTrack(i)

    function setUpdate() {
        
        if (!isNaN(currentTrack.duration)) {
            seek.value = currentTrack.currentTime * (100 / currentTrack.duration);
            
            var currentMinutes = Math.floor(currentTrack.currentTime / 60);
            var currentSeconds = Math.floor(currentTrack.currentTime - currentMinutes * 60);
            var durationMinutes = Math.floor(currentTrack.duration / 60);
            var durationSeconds = Math.floor(currentTrack.duration - durationMinutes * 60);
    
            if (currentSeconds < 10) { currentSeconds = "0" + currentSeconds; }
            if (durationSeconds < 10) { durationSeconds = "0" + durationSeconds; }
            if (currentMinutes < 10) { currentMinutes = "0" + currentMinutes; }
            if (durationMinutes < 10) { durationMinutes = "0" + durationMinutes; }
    
            currentTime.textContent = currentMinutes + ":" + currentSeconds;
            durationTime.textContent = durationMinutes + ":" + durationSeconds;
        }
    }
    function reset() {
        currentTime.textContent = "00:00";
        durationTime.textContent = "00:00";
        seek.value = 0;
        volumeRanger.value = 10;
    }

    const handlePlay=()=>{
        picture.style.animation=""
        play.classList.add("active")
        pause.classList.remove("active")
        currentTrack.pause()
        console.log(currentTrack.duration)
    }

    const handleStop=()=>{
        picture.style.animation="rot 6s linear infinite"
        play.classList.remove("active")
        pause.classList.add("active")
        currentTrack.play()
        handleVolume()
    }
    
    const numberSong=MUSIC_LIST.length

    const handleLeft=()=>{
        i=i-1
        if(i===0){
            i=numberSong
        }
        status1.innerHTML=`Playing music ${i} of ${numberSong}`
        nameSong.innerHTML=MUSIC_LIST[i-1]["name"]
        nameSinger.innerHTML=MUSIC_LIST[i-1]["artist"]
        loadTrack(i)
        handleStop()
    }

    function handleTime(){
        currentTrack.currentTime=currentTrack.duration*(seek.value/100)
        handleStop()
    }

    function handleVolume(){
        currentTrack.volume=volumeRanger.value/100
    }

    const handleRepeat=()=>{
        reset()
        loadTrack(i)
        handleStop()
    }
